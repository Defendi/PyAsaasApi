from setuptools import setup, find_packages

#python3 setup.py build
#python3 setup.py sdist


VERSION = "0.0.1"

setup(
    name="PyAsaasApi",
    version=VERSION,
    author="Alexandre Defendi",
    author_email='alexandre_defendi@hotmail.com',
    description='PyAsaasApi é uma biblioteca de acesso a API da ASAAS',
    long_description=open('README.md', 'r').read(),
    long_description_content_type="text/markdown",

    keywords=['asaas','API','Boleto'],
    classifiers=[
        'Environment :: Plugins',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Lesser General Public License v2 or later (LGPLv2+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    python_requires='>=3.5',
    packages=find_packages(exclude=['*test*']),
    package_data={
        'pyasaasapi': []
    },
    url='https://gitlab.com/Defendi/PyAsaasApi',
    license='LGPL-v2.1+',
    install_requires=[
        'requests',
    ],
    tests_require=[
        'pytest',
    ],
)