#*******************************************************************#
#    ____                                                           #
#   / __ \____  __  _______         © 2021 Alexandre Defendi        #
#  / / / / __ \/ / / / ___/         Created on Mar 01, 2021         #
# / /_/ / /_/ / /_/ (__  )       alexandre_defendi@hotmail.com      #
# \____/ .___/\__,_/____/                                           #   
#     /_/                                                           #
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html) #
#*******************************************************************#
import requests
from .exceptions import NonApiKeyError

class ApiAsaas(object):
    def __init__(self, ambiente=2, token=False):
        self._ambiente = ambiente
        self._token = token
        self.raw_resp = False

    def __repr__(self):
        nm_ambiente = 'Produção' if self._ambiente == 1 else 'Homologação' 
        return 'Api Key %s: %s' % (nm_ambiente, self._token or 'No Key')

    def __unicode__(self):
        return str(self.__repr__())

    def _headers(self):
        if not bool(self._token):
            NonApiKeyError()
        else:
            token = self._token
        return {
            'Content-Type': 'application/json',
            'access_token': '{}'.format(token),
        }

    def _url(self):
        if self._ambiente == 1:
            res = 'https://www.asaas.com/api/v3'
        else:
            res = 'https://sandbox.asaas.com/api/v3'
        return res

    def customer(self, client_id):
        if bool(id):
            resp = requests.get('{url}/customers/{id}'.format(url=self._url(),id=client_id), headers=self._headers())
            self.raw_resp = resp
        
    def customers(self):
        resp = requests.get('{url}/customers'.format(url=self._url()), headers=self._headers())
        self.raw_resp = resp
        
