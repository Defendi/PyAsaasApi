#*******************************************************************#
#    ____                                                           #
#   / __ \____  __  _______         © 2021 Alexandre Defendi        #
#  / / / / __ \/ / / / ___/         Created on Mar 01, 2021         #
# / /_/ / /_/ / /_/ (__  )       alexandre_defendi@hotmail.com      #
# \____/ .___/\__,_/____/                                           #   
#     /_/                                                           #
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html) #
#*******************************************************************#

class NonApiKeyError(Exception):

    def __init__(self):
        self.message = 'Insira a API Key'

    def __str__(self):
        return self.message
