# Alexandre Defendi
# {'object': 'list', 'hasMore': False, 'totalCount': 1, 'limit': 10, 'offset': 0, 'data': [{'object': 'customer', 'id': 'cus_000004659855', 'dateCreated': '2021-04-13', 'name': 'Cliente de teste', 'email': 'alexandre@opussystem.com.br', 'company': None, 'phone': None, 'mobilePhone': '41992705320', 'address': 'Rua São Jorge do Oeste', 'addressNumber': '345', 'complement': None, 'province': 'Cidade Industrial', 'postalCode': '81260100', 'cpfCnpj': '68150703000130', 'personType': 'JURIDICA', 'deleted': False, 'additionalEmails': None, 'externalReference': None, 'notificationDisabled': False, 'observations': None, 'city': 13405, 'state': 'PR', 'country': 'Brasil', 'foreignCustomer': False}]}

import requests

resp = requests.get('https://sandbox.asaas.com/api/v3/customers',headers={'Content-Type':'application/json','access_token':'9cf04cc9b582aae836d2b3c0b4a2da4da4248cb86b18d760c2f03bbda136c015'})

print(resp)
print(resp.json())
